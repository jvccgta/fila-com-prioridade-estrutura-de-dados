#include <iostream>
#include "fila.h"

using namespace std;

FilaDinamicaComPrioridade cria ()
{
    FilaDinamicaComPrioridade fila;
    fila.altaPrioridade = NULL;
    fila.baixaPrioridade = NULL;
    fila.mediaPrioridade = NULL;
    fila.numElementos = 0;
    return fila;
}

void destroi(FilaDinamicaComPrioridade fila){
    struct nodo *a;
    while(fila.altaPrioridade != NULL){
        a = fila.altaPrioridade;
        fila.altaPrioridade = fila.altaPrioridade->proximo;
        delete a;
    }
    
    while(fila.baixaPrioridade != NULL){
        a = fila.baixaPrioridade;
        fila.baixaPrioridade = fila.baixaPrioridade->proximo;
        delete a;
    }
    
    while(fila.mediaPrioridade != NULL){
        a = fila.mediaPrioridade;
        fila.mediaPrioridade = fila.mediaPrioridade->proximo;
        delete a;
    }
    
    fila.numElementos = 0;
}

bool ehVazia (FilaDinamicaComPrioridade fila){
    return fila.numElementos == 0;
}

int numeroDeElementos (FilaDinamicaComPrioridade fila){
    return fila.numElementos;
}

bool pesquisaPeloPonteiro(struct nodo *a, char elemento){
    while(a != NULL)
    {
        if(a->elemento == elemento)
        {
            return true;
        }
        else
        {
            a = a->proximo;
        }
    }
    return false;
}

bool existeElemento (FilaDinamicaComPrioridade fila, char elemento){ 
    if(pesquisaPeloPonteiro(fila.altaPrioridade,elemento)) return true;
    if(pesquisaPeloPonteiro(fila.baixaPrioridade,elemento)) return true;
    if(pesquisaPeloPonteiro(fila.mediaPrioridade,elemento)) return true;
    return false;
}

void pesquisaPelaPosicao(int posicao, struct nodo **a, int &posicaoCorrente){
    while(posicaoCorrente<posicao and *a!=NULL){
        *a = (*a)->proximo;
        posicaoCorrente++;
    }  
}

char umElemento ( FilaDinamicaComPrioridade fila, int posicao){
    int posicaoCorrente = 1;
    
    struct nodo *a = fila.altaPrioridade;
    if(a!=NULL){
        pesquisaPelaPosicao(posicao,&a,posicaoCorrente);
        if(posicao==posicaoCorrente and a!=NULL) return a->elemento;
    }
    
    a = fila.mediaPrioridade;
    if(a!=NULL){
        pesquisaPelaPosicao(posicao,&a,posicaoCorrente);
        if(posicao==posicaoCorrente and a!=NULL) return a->elemento;
    }
    
    a = fila.baixaPrioridade;
    if(a!=NULL){
        pesquisaPelaPosicao(posicao,&a,posicaoCorrente);
        return a->elemento;
    }
}

void pesquisaPeloElemento(char elemento, struct nodo **a, int &posicao){
    while(*a!=NULL and (*a)->elemento!=elemento){
        *a = (*a)->proximo;
        posicao++; 
    }
}

int posicao (FilaDinamicaComPrioridade fila, char elemento){
    int posicao = 1;
    
    struct nodo *a = fila.altaPrioridade;
    pesquisaPeloElemento(elemento,&a,posicao);
    if(a!=NULL and a->elemento == elemento) return posicao;
    
    a = fila.mediaPrioridade;
    pesquisaPeloElemento(elemento,&a,posicao);
    if(a!=NULL and a->elemento == elemento) return posicao;
    
    
    a = fila.baixaPrioridade;
    pesquisaPeloElemento(elemento,&a,posicao);
    return posicao;
}

char primeiroElemento(FilaDinamicaComPrioridade fila){
    if(fila.altaPrioridade!=NULL) return fila.altaPrioridade->elemento;
    if(fila.mediaPrioridade!=NULL) return fila.mediaPrioridade->elemento;
    if(fila.baixaPrioridade!=NULL) return fila.baixaPrioridade->elemento;
}

void ultimoElementoDaPrioridade(struct nodo **a){
    while((*a)->proximo!=NULL){
            *a = (*a)->proximo;
    }
}

char ultimoElemento(FilaDinamicaComPrioridade fila){
    struct nodo *a;
    if(fila.altaPrioridade!=NULL){
        a = fila.altaPrioridade;
        ultimoElementoDaPrioridade(&a);
    }
    if(fila.mediaPrioridade!=NULL){
        a = fila.mediaPrioridade;
        ultimoElementoDaPrioridade(&a);
    }
    if(fila.baixaPrioridade!=NULL){
        a = fila.baixaPrioridade;
        ultimoElementoDaPrioridade(&a);
    }
    return a->elemento;
}

void insereNoFinal(struct nodo **a, char elemento){
    struct nodo *pAuxiliar, *pAnterior;
    pAuxiliar = new nodo;
    if(pAuxiliar==NULL) return;
    pAuxiliar->elemento = elemento;
    pAuxiliar->proximo = NULL;

    if(*a==NULL){
        *a = pAuxiliar;
    }else{
        pAnterior = *a;
        while(pAnterior->proximo != NULL) pAnterior = pAnterior->proximo;
        pAnterior->proximo = pAuxiliar;
    }
}

void insere (FilaDinamicaComPrioridade &fila, char elemento, char prioridade){
    prioridade = toupper(prioridade);
    if(prioridade != 'A' and prioridade !='B' and prioridade!='M') return; // if se for passado prioridade errada
    if(prioridade == 'B') insereNoFinal(&fila.baixaPrioridade,elemento);
    if(prioridade == 'M') insereNoFinal(&fila.mediaPrioridade,elemento);
    if(prioridade == 'A') insereNoFinal(&fila.altaPrioridade,elemento);
    fila.numElementos++;
}

void retiraDaPrioridade(struct nodo **a){
    struct nodo *aux = *a;
    *a = (*a)->proximo;
    delete aux;
}

void retira(FilaDinamicaComPrioridade &fila){
    if(ehVazia(fila)) return;
    if(fila.altaPrioridade!=NULL) retiraDaPrioridade(&fila.altaPrioridade);
    else if(fila.mediaPrioridade!=NULL) retiraDaPrioridade(&fila.mediaPrioridade);
    else retiraDaPrioridade(&fila.baixaPrioridade);
    fila.numElementos--;
}

void mostraPorPrioridade(struct nodo *a){
    while(a != NULL){
        cout << a->elemento << " ";
        a = a->proximo;
    }  
}

void mostra (FilaDinamicaComPrioridade fila){
    cout << "Alta: ";
    mostraPorPrioridade(fila.altaPrioridade);
    cout << endl << "Media: ";
    mostraPorPrioridade(fila.mediaPrioridade);
    cout << endl << "Baixa: ";
    mostraPorPrioridade(fila.baixaPrioridade);
}

