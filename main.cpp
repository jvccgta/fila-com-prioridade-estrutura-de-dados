#include <iostream>
#include "fila.h"

using namespace std;


void opcoes(){
    cout <<"Escolha a op��o desejada: " << endl
    <<"1 - Cria" << endl
    <<"2 - Insere" << endl
    <<"3 - Retira" << endl
    <<"4 - Mostra" << endl
    <<"5 - Verifica se esta vazia" << endl
    <<"6 - N�mero de elementos" << endl
    <<"7 - Procura elemento pela posi��o" << endl
    <<"8 - Procura posi��o do elemento " << endl
    <<"9 - Primeiro elemento da fila" << endl
    <<"10 - �ltimo elemento da fila" << endl
    <<"11 - Sair"<< endl;
}

void insereNaFila(FilaDinamicaComPrioridade &fila){
    char elemento, prioridade,nome;
    int id;
    cout <<"Insira o nome do elemento:" << endl;
    cin >> elemento;
    do{
        cout <<"Insira a prioridade alta(A), media(M) ou baixa(B):\n" <<endl;
        cin >> prioridade;
        prioridade = toupper(prioridade);
        if(prioridade!='B' and prioridade!='M' and prioridade!='A') 
            cout <<"Prioridade inv�lida, insira novamente"<< endl;
    }while(prioridade!='B' and prioridade!='M' and prioridade!='A');
    insere(fila,elemento,prioridade);
}

void retiraDaFila(FilaDinamicaComPrioridade &fila){
    cout << "Elemento Retirado "<< primeiroElemento(fila) << endl;
    retira(fila);
}

void mostraFila(FilaDinamicaComPrioridade fila){
    mostra(fila);
    cout << endl;
}

void filaVazia(FilaDinamicaComPrioridade fila){
    if(ehVazia(fila)) cout <<"Est� vazia" << endl;
    else cout <<"N�o est� vazia" << endl;
}

void numeroDeElementosDaFila(FilaDinamicaComPrioridade fila){
    cout << "O n�mero de elementos �: " << numeroDeElementos(fila) << endl;
}

void procuraElementoPelaPosicao(FilaDinamicaComPrioridade fila){
    int posicao;
    do{
        cout <<"Informe a posi��o desejada"<< endl;
        cin >> posicao;
        if(posicao < 1 and posicao > fila.numElementos) cout << "Posi��o inv�lida, Digite novamente." << endl;
    }while(posicao < 1 and posicao > fila.numElementos);
    cout << "Elemento: " << umElemento(fila,posicao) << endl;
}

void procuraPosicaoDoElemento(FilaDinamicaComPrioridade fila){
    char elemento;
    do{
        cout << "Insira o elemento que deseja pesquisar:" << endl;
        cin >> elemento;
        if(!existeElemento(fila,elemento))
            cout << "Elemento n�o existe na fila, insira novamente." << endl;
    }while(!existeElemento(fila,elemento));
    cout << "A posi��o do elemento �: " << posicao(fila,elemento) << endl;  
}

void primeiroElementoDaFila(FilaDinamicaComPrioridade fila){
    cout << "Primeiro elemento: " << primeiroElemento(fila) << endl;
}

void ultimoElementoDaFila(FilaDinamicaComPrioridade fila){
    cout << "�ltimo elemento: " << ultimoElemento(fila) << endl;
}

void menu(int opcao, FilaDinamicaComPrioridade &fila){
    switch (opcao){
        case 1:
            fila = cria();
            break;
        case 2:
            insereNaFila(fila);
            break;
        case 3:
            retiraDaFila(fila);
            break;
        case 4:
            mostraFila(fila);
            break;
        case 5:
            filaVazia(fila);
            break;
        case 6:
            numeroDeElementosDaFila(fila);
            break;
        case 7:
            procuraElementoPelaPosicao(fila);
            break;
        case 8:
            procuraPosicaoDoElemento(fila);
            break;
        case 9:
            primeiroElementoDaFila(fila);
            break;
        case 10:
            ultimoElementoDaFila(fila);
            break;    
    }
}

/*
 * 
 */
int main(int argc, char** argv) {
	setlocale(LC_ALL,"Portuguese");
    FilaDinamicaComPrioridade fila;
    int opcao;
    do{
        opcoes();
        cin >> opcao;
        menu(opcao,fila);
        if(opcao < 1 and opcao > 11)
            cout << "Opcao inv�lida, digite novamente" << endl;
    }while(opcao!=11);
    destroi(fila);
    return 0;
}


