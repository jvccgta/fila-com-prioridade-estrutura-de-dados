#ifndef FILA_H
#define FILA_H

struct nodo {
    char elemento;
    struct nodo *proximo; 
};  

typedef struct {     
	char nome;
    int numElementos;     
    struct nodo *baixaPrioridade, *mediaPrioridade, *altaPrioridade; 
}FilaDinamicaComPrioridade;

FilaDinamicaComPrioridade cria();
void destroi(FilaDinamicaComPrioridade fila);
bool ehVazia (FilaDinamicaComPrioridade fila);
int numeroDeElementos (FilaDinamicaComPrioridade fila);
bool existeElemento (FilaDinamicaComPrioridade fila, char elemento);
char umElemento ( FilaDinamicaComPrioridade fila, int posicao);
int posicao (FilaDinamicaComPrioridade fila, char elemento);
char primeiroElemento(FilaDinamicaComPrioridade fila);
char ultimoElemento(FilaDinamicaComPrioridade fila);
void insere (FilaDinamicaComPrioridade &fila, char elemento, char prioridade);
void retira(FilaDinamicaComPrioridade &fila);
void mostra (FilaDinamicaComPrioridade fila);


#endif /* FILA_H */

